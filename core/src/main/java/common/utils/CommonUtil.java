package common.utils;

import org.apache.commons.lang.RandomStringUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2017/6/6 0006.
 */
public class CommonUtil {
    private static SimpleDateFormat sdf_birthdy = new SimpleDateFormat("MMdd");
    private static SimpleDateFormat sdf_day = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat sdf_month = new SimpleDateFormat("yyyyMM");
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private static DecimalFormat df = new DecimalFormat("0.00");
    public static SimpleDateFormat sdf_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * @Purpose   生成分页信息
     * @param
     * @version   1.0
     * @author    lizhun
     * @time      2017/6/9 0009 下午 4:11
     * @return    Map
     */
    public static Map<String, Object> page(int total_record , int current_page , int page_size){
        int total_page;
        if (total_record % page_size == 0) {
            total_page = total_record/page_size;
        } else {
            total_page = (total_record/page_size) + 1;
        }
        if (current_page >= total_page) {
            current_page = total_page;
        }
        if (current_page <= 1) {
            current_page = 1;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("current_page", (current_page - 1) * page_size);
        map.put("page_size", page_size);
        map.put("total_record", total_record);
        map.put("total_page",total_page);
        return map;
    }
    /**
     * @return String
     * @Purpose 生成token
     * @author lizhun
     * @version 1.0
     */
    public static String getToken() {
        UUID token = UUID.randomUUID();
        return "" + token;
    }

    /**
     * @Purpose  获取编号
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static String getOrderNub() {
        Date date = new Date();
        String orderNub = sdf.format(date).substring(2, 12);

        String code = "";
        while(code.length() < 5)
        {
            code+=(int)(Math.random() * 10);
        }
        orderNub = orderNub + code;
        return orderNub;
    }

    /**
     * @return String
     * @Purpose 获取当前天数
     * @author lizhun
     * @version 1.0
     */
    public static String getDay() {
        Date date = new Date();
        return sdf_day.format(date);
    }
    /**
     * @return String
     * @Purpose 获取当前月份
     * @author lizhun
     * @version 1.0
     */
    public static String getMonth() {
        Date date = new Date();
        return sdf_month.format(date);
    }
    /**
     * @return String
     * @Purpose 获取当前月份
     * @author lizhun
     * @version 1.0
     */
    public static String getBirthday() {
        Date date = new Date();
        return sdf_birthdy.format(date);
    }
    /**
     * @Purpose  获取四位随机数
     * @author   lizhun
     * @param    length
     * @version  1.0
     * @return   String
     */
    public static String randomString(int length) {
        return RandomStringUtils.random(length, false, true);
    }
    /**
     * @Purpose  获取用户id
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static Long getMembId() {
        int random = (int)(Math.random() * 100000000);
        if (random < 100000000) {
            random = random * 10;
        }
        return new Long(random);
    }

    /**
     * @Purpose  计算每百分收益
     * @author   lizhun
     * @param    annual_rate
     * @param    loan_time
     * @version  1.0
     * @return   String
     */
    public static Long getInterest(Integer annual_rate, Integer loan_time) {
        Double result = Double.valueOf(df.format((double)annual_rate * loan_time  / 365));
        return result.longValue() ;
    }
    /**
     * @Purpose  计算天数
     * @param    reapyment_time
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static int getLoanTime(Date reapyment_time) {
        Date now = new Date();
        int days = (int)((reapyment_time.getTime() - now.getTime())/86400000);
        return days;
    }
    /**
     * @Purpose  天数计算
     * @param    days
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static Date datePlus(int days) {
        Date result = null;
        try {
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, days);
            Date date = cal.getTime();
            String dates = sdf_day.format(date) + "235959";
            result = sdf.parse(dates);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;

    }
    /**
     * @Purpose  天数计算
     * @param    days
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static Date datePlusStart(int days) {
        Date result = null;
        try {
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, days);
            Date date = cal.getTime();
            String dates = sdf_day.format(date) + "000000";
            result = sdf.parse(dates);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;

    }
    /**
     * @return String
     * @Purpose 获取活动id
     * @author lizhun
     * @version 1.0
     */
    public static Integer getActivityId() {
        Date date = new Date();
        String activity_id = sdf_day.format(date);

        String code = "";
        while(code.length() < 2)
        {
            code+=(int)(Math.random() * 10);
        }
        activity_id = activity_id + code;
        return Integer.valueOf(activity_id);
    }
    /**
     * @Purpose  获取前一天日期
     * @param
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static String getDayBefore(int day_size) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE,day - day_size);
        return sdf_day.format(c.getTime());

    }
    /**
     * @Purpose  获取前一天日期
     * @param
     * @author   lizhun
     * @version  1.0
     * @return   String
     */
    public static String getDayBeforeMonth(int day_size) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE,day - day_size);
        return sdf_month.format(c.getTime());

    }
}
