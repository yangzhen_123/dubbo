package com.cloud.consum.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RibbonServiceImpl implements RibbonService {
    @Autowired
    RestTemplate restTemplate;
    //熔断和降级
    @HystrixCommand(fallbackMethod = "addServiceFallback")
    public String addService(){
        return restTemplate.getForEntity("http://demo-service/add?a=10&b=20",String.class).getBody();
    }
    public String addServiceFallback(){
        return "error";
    }

    @HystrixCommand(fallbackMethod = "listDict")
    public String findDictList() {
        return restTemplate.getForEntity("http://service-a/find/dict/list",String.class).getBody();
    }

    public String listDict(){
        return "dict list is error";
    }
}
