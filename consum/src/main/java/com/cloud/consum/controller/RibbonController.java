package com.cloud.consum.controller;

import com.cloud.consum.service.RibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RibbonController {
    @Autowired
    private RibbonService ribbonService;

    @RequestMapping(value = "/add",method = {RequestMethod.GET, RequestMethod.POST})
    public String add(){
        return ribbonService.addService();
    }
    @RequestMapping(value = "/test",method = {RequestMethod.GET, RequestMethod.POST})
    public String test(){
        return "ss";
    }
    @RequestMapping(value = "/find",method = {RequestMethod.GET, RequestMethod.POST})
    public String findDictList() {
        String result= ribbonService.findDictList();
        return result;
    }
}
