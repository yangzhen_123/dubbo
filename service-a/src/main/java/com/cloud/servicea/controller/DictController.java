package com.cloud.servicea.controller;

import com.cloud.servicea.bean.Dict;
import com.cloud.servicea.service.DictService;
import com.netflix.discovery.DiscoveryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DictController {
    @Autowired
    private org.springframework.cloud.client.discovery.DiscoveryClient discoveryClient;
    @Autowired
    private DictService dictService;
    @RequestMapping(value = "/add" ,method = RequestMethod.GET)
    public Integer add(@RequestParam Integer a, @RequestParam Integer b) {
        ServiceInstance instance = discoveryClient.getLocalServiceInstance();
        Integer r = a + b;
        return r;
    }

    @RequestMapping(value = "/test")
    public String test (){
        return "test";
    }

    @RequestMapping(value = "/find/dict/list")
    public List<Dict> findDictList(){
        return dictService.findDictList();
    }

    public  void  updateDictById(@RequestParam(value = "dict_id") Long dict_id,@RequestParam(value = "dict_sort") Integer dict_sort) {
        dictService.updateDictById(dict_id,dict_sort);
    }
}
