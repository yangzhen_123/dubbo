package com.cloud.servicea.mapper;


import com.cloud.servicea.bean.Dict;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictMapper {
    @Select("select * from dict ")
    List<Dict> findDictList();

    @Select("select * from dict dict_id=#{dict_id} ")
    Dict findDiceById(@Param(value = "dict_id") Long dict_id);

    @Update("update dict set dict_sort=#{dict_sort} where dict_id = #{dict_id} ")
    void updateDictById(@Param(value = "dict_id") Long dict_id, @Param(value = "dict_sort") Integer dict_sort);
}
