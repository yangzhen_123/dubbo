package com.cloud.servicea.service;


import com.cloud.servicea.bean.Dict;

import java.util.List;

public interface DictService {
    List<Dict> findDictList();

   void updateDictById(Long dict_id, Integer dict_sort);
}
