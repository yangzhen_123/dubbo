package com.cloud.servicea.service;

import com.cloud.servicea.bean.Dict;
import com.cloud.servicea.mapper.DictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class DictServiceImpl implements DictService {
    @Autowired
    private DictMapper dictMapper;
    @Override
    public List<Dict> findDictList() {
        return dictMapper.findDictList();
    }

    @Override
    public void updateDictById(Long dict_id, Integer dict_sort) {
        dictMapper.updateDictById(dict_id,dict_sort);
    }
}
