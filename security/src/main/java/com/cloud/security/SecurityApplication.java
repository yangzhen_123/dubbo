package com.cloud.security;

import com.cloud.security.entity.Account;
import com.cloud.security.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration()
public class SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}
    //-----------------下面代码处理初始化一个用户-------------
    //用户名:leftso 用户密码:111aaa 用户角色:ROLE_USER
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    public void init(){
        try {
            Account account=new Account();
            account.setName("user");
            account.setPassword("123456");
            account.setRoles(new String []{"ROLE_USER"});
            accountRepository.deleteAll();
            accountRepository.save(account);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
